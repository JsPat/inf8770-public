from LZ77 import LZ77Compressor
from os import listdir
from os.path import isfile, join, getsize
from dahuffman import HuffmanCodec
from prettytable import PrettyTable
import time
import statistics

# define the compressor
compressor = LZ77Compressor()

def compress(paths):
    """
    compress compress all files in the given path with LZ77 and huffman

    return 
        compressionLZ77 (byte) - the number of Byte after compression using LZ77 for each files
        timeLZ77 (s) - the total time for the compression using LZ77 for each files

        compressionHuffman (byte) - the number of Byte after compression using Huffman for each files
        timeHuffman (s) - the total time for the compression using Huffman for each files
    """
    

    compressionLZ77 = []
    timeLZ77 = []
    compressionHuffman = []
    timeHuffman = []

    for p in paths:
        ############################# LZ77  #############################
        start_time = time.time()
        compressionLZ77.append(len(compressor.compress(p)))
        timeLZ77.append(time.time() - start_time)


        ########################### HUFFMAN  ############################
        start_time = time.time()
        f = open(p, "rb")
        data = f.read()
        f.close()

        codec = HuffmanCodec.from_data(data)
        compressionHuffman.append(len(codec.encode(data)))
        timeHuffman.append(time.time() - start_time)
        

    return compressionLZ77, timeLZ77, compressionHuffman, timeHuffman

def analyse(path, title):
    """
    analyse analyse the statistics of all file contain in the given path
    this function print in the console the resulting table
    """
    allPaths = [join(path, f) for f in listdir(path) if isfile(join(path, f))]

    compLZ77, timeLZ77, compHuffman, timeHuffman = compress(allPaths)

    table = PrettyTable(["File", "Initial file size (bytes)", "Final file size LZ77 (bytes)", "LZ77 ratio (%)", "LZ77 compression time (s)", "Final file size Huffman (bytes)", "Huffman ratio (%)", "Huffman compression time (s)"])
    table.align["File"] = "l"
    table.align["Initial file size (bytes)"] = "l"
    table.align["Final file size LZ77 (bytes)"] = "l"
    table.align["LZ77 ratio (%)"] = "l"
    table.align["LZ77 compression time (s)"] = "l"
    table.align["Final file size Huffman (bytes)"] = "l"
    table.align["Huffman ratio (%)"] = "l"
    table.align["Huffman compression time (s)"] = "l"

    allRatioLZ77 = []
    allRatioHuffman = []

    for i in range(len(allPaths)):
        size = getsize(allPaths[i]) * 8
        ratioLZ77 = compLZ77[i]/size * 100
        ratioHuffman = compHuffman[i]/size * 100

        allRatioLZ77.append(ratioLZ77)
        allRatioHuffman.append(ratioHuffman)

        table.add_row([allPaths[i],
            size,
            compLZ77[i],
            round(ratioLZ77, 4),
            round(timeLZ77[i], 4),
            compHuffman[i],
            round(ratioHuffman, 4),
            round(timeHuffman[i], 4)
        ])

    # mean of all colums
    table.add_row(["Mean", 
        "-",
        "-",
        round(statistics.mean(allRatioLZ77), 4),
        round(statistics.mean(timeLZ77), 4),
        "-",
        round(statistics.mean(allRatioHuffman), 4),
        round(statistics.mean(timeHuffman), 4)
    ])

    # standar deviation of all command
    table.add_row(["Standard deviation",
        "-",
        "-",
        round(statistics.stdev(allRatioHuffman),4 ),
        round(statistics.stdev(timeLZ77),4 ),
        "-",
        round(statistics.stdev(allRatioHuffman),4 ),
        round(statistics.stdev(timeHuffman), 4 ),
    ])
    print(table.get_string(title=title, sortby="File"))



# files
imgPath = "../imgTest/"
txtPath = "../txtTest"

analyse(txtPath, "text files")
print("\n\n\n")
analyse(imgPath, "Images")

