import cv2
import numpy as np


def img_to_rgb_floats(img):
    img = img.astype(float)
    r = img[:, :, 2]
    g = img[:, :, 1]
    b = img[:, :, 0]
    return r, g, b


def make_multiple_of_n(image, n):
    x_zero_pad_length = 0
    y_zero_pad_length = 0
    if image.shape[0] % n != 0:
        x_zero_pad_length = n - image.shape[0] % n
    if image.shape[1] % n != 0:
        y_zero_pad_length = n - image.shape[1] % n
    if x_zero_pad_length != 0 or y_zero_pad_length != 0:
        if len(image.shape) == 2:
            pad_image = np.zeros((image.shape[0] + x_zero_pad_length, image.shape[1] + y_zero_pad_length))
        elif len(image.shape) == 3:
            pad_image = np.zeros((image.shape[0] + x_zero_pad_length, image.shape[1] + y_zero_pad_length, image.shape[2]))
        else:
            print('invalid shape for image')
        pad_image[:image.shape[0], :image.shape[1], ...] = image
        image = pad_image
    return image


def rgb_to_YCbCr(r, g, b):
    Y = 0.299*r + 0.587*g + 0.114*b
    Cb = 128 + 0.564*(b - Y)
    Cr = 128 + 0.713*(r - Y)
    return Y, Cb, Cr


def YCbCr_to_rgb(Y, Cb, Cr):
    r = Y + 1.403*(Cr - 128)
    g = Y - 0.714*(Cr - 128) - 0.344*(Cb - 128)
    b = Y + 1.773*(Cb - 128)
    return r, g, b


def rgb_to_YUV(r, g, b):
    Y = (r + 2 * g + b) / 4
    U = b - g
    V = r - g
    return Y, U, V


def YUV_to_rgb(Y, U, V):
    g = Y - ((U + V) / 4)
    r = V + g
    b = U + g
    return r, g, b


def float_to_image(float_matrix):
    return np.clip(float_matrix, 0, 255).astype(np.uint8)


def subsample(first, second, third, subsample):
    if subsample == '4:2:0':
        second = cv2.resize(second, (int(second.shape[1] / 2), int(second.shape[0] / 2)))
        third = cv2.resize(third, (int(third.shape[1] / 2), int(third.shape[0] / 2)))
    elif subsample == '4:2:2':
        second = cv2.resize(second, (int(second.shape[1] / 2), int(second.shape[0])))
        third = cv2.resize(third, (int(third.shape[1] / 2), int(third.shape[0])))
    elif subsample == '4:1:1':
        second = cv2.resize(second, (int(second.shape[1] / 4), int(second.shape[0])))
        third = cv2.resize(third, (int(third.shape[1] / 4), int(third.shape[0])))
    return first, second, third


def upsample(first, second, third, subsample):
    if subsample == '4:2:0':
        second = cv2.resize(second, (second.shape[1] * 2, second.shape[0] * 2))
        third = cv2.resize(third, (third.shape[1] * 2, third.shape[0] * 2))
    elif subsample == '4:2:2':
        second = cv2.resize(second, (int(second.shape[1] * 2), int(second.shape[0])))
        third = cv2.resize(third, (int(third.shape[1] * 2), int(third.shape[0])))
    elif subsample == '4:1:1':
        second = cv2.resize(second, (int(second.shape[1] * 4), int(second.shape[0])))
        third = cv2.resize(third, (int(third.shape[1] * 4), int(third.shape[0])))
    else:
        print('Le subsampling ', subsample, ' n\'est pas implémenté')
    return first, second, third


def array_to_nxn_blocks(array, n=8):
    assert array.shape[0] % n == 0 and array.shape[1] % n == 0, "array is not a multiple of n"
    list_of_blocks = []
    for i in range(0, array.shape[0], n):
        for j in range(0, array.shape[1], n):
            list_of_blocks.append(array[i:i+n, j:j+n])
    return list_of_blocks


def nxn_blocks_to_array(list_of_blocks, n_x, n_y, n=8):
    array = np.zeros((n_x*n, n_y*n))
    count = 0
    for i in range(n_x):
        for j in range(n_y):
            array[i*n:i*n+n, j*n:j*n+n] = list_of_blocks[count]
            count += 1
    return array


def zigzag(block, n=8):
    elements = []
    i = 0
    j = 0
    elements.append(block[i, j])

    while len(elements) < n*n:
        if j < n-1:
            j += 1
        else:
            i += 1

        while i < n and j >= 0:
            elements.append(block[i, j])
            i += 1
            j -= 1
        i -= 1
        j += 1
        if i < n - 1:
            i += 1
        else:
           j += 1
        while i >= 0 and j < n:
            elements.append(block[i, j])
            i -= 1
            j += 1
        i += 1
        j -= 1
    return elements


def zigzag_inverse(elements, n=8):
    block = np.zeros((n, n))
    i = 0
    j = 0
    count = 0

    block[i, j] = elements[count]
    count += 1

    while count < n*n:
        if j < n-1:
            j += 1
        else:
            i += 1

        while i < n and j >= 0:
            block[i, j] = elements[count]
            count += 1
            i += 1
            j -= 1
        i -= 1
        j += 1
        if i < n - 1:
            i += 1
        else:
           j += 1
        while i >= 0 and j < n:
            block[i, j] = elements[count]
            count += 1
            i -= 1
            j += 1
        i += 1
        j -= 1
    return block
