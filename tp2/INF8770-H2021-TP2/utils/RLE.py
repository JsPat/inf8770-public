# adapted from: https://stackabuse.com/run-length-encoding/
import math


def rle_encode(data, max_length_for_sequence):
    encoding = ''
    prev_char = ''
    count = 1
    bits_per_count = math.ceil(math.log(max_length_for_sequence, 2))
    if not data: return ''

    for char in data:
        # If the prev and current characters
        # don't match...
        if char != prev_char or count == max_length_for_sequence:
            # ...then add the count and character
            # to our encoding
            if prev_char:
                encoding += "{0:b}".format(count).zfill(bits_per_count) + prev_char
            count = 1
            prev_char = char
        else:
            # Or increment our counter
            # if the characters do match
            count += 1
    else:
        # Finish off the encoding
        encoding += "{0:b}".format(count).zfill(bits_per_count) + prev_char
        return encoding
