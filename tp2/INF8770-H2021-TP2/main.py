from jpeg import jpeg
from jpeg2000 import jpeg2000
from image_quality import image_quality
import numpy as np

# Inspire de la matrix de quantification dans les notes de cours
# et du code de quantification des notes de cours
quantification_matrix = np.matrix('16 11 10 16 24 40 51 61;\
12 12 14 19 26 58 60 55;\
14 13 16 24 40 57 69 56;\
14 17 22 29 51 87 80 62;\
18 22 37 56 68 109 103 77;\
24 35 55 64 81 104 103 92;\
49 64 78 77 103 121 120 101;\
72 92 95 98 112 100 103 99').astype('float')


def question6_1():
    jpeg('4:2:0', 'data/RGB.jpg', "./test_jpeg_iteration6_1.jpg", quantification_matrix)
    image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_1.jpg", "JPEG iteration 0")
    for i in range(1, 10):
        jpeg('4:2:0', './test_jpeg_iteration6_1.jpg', "./test_jpeg_iteration6_1.jpg", quantification_matrix)
        image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_1.jpg", f"JPEG iteration {i}")


def question6_2():
    jpeg('4:2:0', 'data/RGB.jpg', "./test_jpeg_iteration6_2.jpg", quantification_matrix)
    image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_2.jpg", "JPEG iteration 0")
    for i in range(1, 10):
        quantification_matrix_random = np.random.rand(8,8).astype('float')
        jpeg('4:2:0', './test_jpeg_iteration6_2.jpg', "./test_jpeg_iteration6_2.jpg", quantification_matrix_random)
        image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_2.jpg", f"JPEG iteration {i}")

def question6_3():
    jpeg('4:2:0', 'data/RGB.jpg', "./test_jpeg_iteration6_3.jpg", quantification_matrix)
    image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_3.jpg", "JPEG et JPEG2000 iteration 0")
    for i in range(1, 10):
        if (i % 2) == 0:
            jpeg('4:2:0', './test_jpeg_iteration6_3.jpg', "./test_jpeg_iteration6_3.jpg", quantification_matrix)
        else:
            jpeg2000('4:2:0', "./test_jpeg_iteration6_3.jpg", "./test_jpeg_iteration6_3.jpg")
        image_quality("./data/RGB.jpg", "./test_jpeg_iteration6_3.jpg", f"JPEG et JPEG2000 iteration {i}")

if __name__ == "__main__":
    print("JPEG et JPEG2000 avec '4:2:0'")
    jpeg('4:2:0', "./data/RGB.jpg", "./test_jpeg_4-2-0.jpg", quantification_matrix)
    jpeg2000('4:2:0', "./data/RGB.jpg", "./test_jpeg2000_4-2-0.jpg")
    image_quality("./data/RGB.jpg", "./test_jpeg_4-2-0.jpg", "JPEG")
    image_quality("./data/RGB.jpg", "./test_jpeg2000_4-2-0.jpg", "JPEG2000")

    print("-----------------------------")
    print("JPEG et JPEG2000 avec '4:2:2'")
    jpeg('4:2:2', "./data/RGB.jpg", "./test_jpeg_4-2-2.jpg", quantification_matrix)
    image_quality("./data/RGB.jpg", "./test_jpeg_4-2-2.jpg", "JPEG")
    image_quality("./data/RGB.jpg", "./test_jpeg2000_4-2-2.jpg", "JPEG2000")

    print("-----------------------------")
    print("question 6 partie 1")
    question6_1()

    print("-----------------------------")
    print("question 6 partie 2")
    question6_2()

    print("-----------------------------")
    print("question 6 partie 3")
    question6_3()
