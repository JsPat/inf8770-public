import numpy as np
import cv2
import os
from utils import utils
from utils import Huffman
from utils import RLE
import scipy.fftpack as dctpack

def dct(blocks):
    # Inspire du code de DCT des notes de cours
    for i, _ in enumerate(blocks):
        blocks[i] = blocks[i] - 128
        blocks[i] = dctpack.dct(dctpack.dct(blocks[i], axis=0, norm='ortho'), axis=1, norm='ortho')
    return blocks

def dct_inv(blocks):
    # Inspire du code de DCT des notes de cours
    for i, _ in enumerate(blocks):
        blocks[i] = dctpack.idct(dctpack.idct(blocks[i], axis=0, norm='ortho'), axis=1, norm='ortho')
        blocks[i] = blocks[i] + 128
    return blocks

def quantification(blocks, quantification_matrix):
    for i, _ in enumerate(blocks):
        blocks[i] = np.round(np.divide(blocks[i], quantification_matrix))
    return blocks

def quantification_inv(blocks, quantification_matrix):
    for i, _ in enumerate(blocks):
        blocks[i] = np.round(np.multiply(blocks[i], quantification_matrix))
    return blocks

def jpeg(SUBSAMPLING, image_path, output_image_path, quantification_matrix):
    # On s'assure que l'image existe
    assert os.path.exists(image_path), "L'image n'existe pas"
    # On lit l'image
    image = cv2.imread(image_path)
    image_original = image
    # largeur et hauteur
    w, h = image.shape[:2]
    # On fait un zero padding pour s'assurer que l'image est un multiple de 16
    image = utils.make_multiple_of_n(image, 16)
    # Chaque channel en tant que floats
    r, g, b = utils.img_to_rgb_floats(image)
    # Conversion YCbCr
    Y, Cb, Cr = utils.rgb_to_YCbCr(r, g, b)
    # Subsample
    Y, Cb, Cr = utils.subsample(Y, Cb, Cr, subsample=SUBSAMPLING)
    # On obtient une liste de blocs 8x8
    Y_blocks = utils.array_to_nxn_blocks(Y, n=8)
    Cb_blocks = utils.array_to_nxn_blocks(Cb, n=8)
    Cr_blocks = utils.array_to_nxn_blocks(Cr, n=8)

    # TODO DCT
    Y_blocks = dct(Y_blocks)
    Cb_blocks = dct(Cb_blocks)
    Cr_blocks = dct(Cr_blocks)

    # TODO quantification
    Y_blocks = quantification(Y_blocks, quantification_matrix)
    Cb_blocks = quantification(Cb_blocks, quantification_matrix)
    Cr_blocks = quantification(Cr_blocks, quantification_matrix)

    # On aplatit les blocs avec un pattern en zig-zag
    # TODO remplacer Y_blocks par vos blocs quantifiés, même chose pour Cb, Cr
    Y_flattened_blocks = [utils.zigzag(block) for block in Y_blocks]
    Cb_flattened_blocks = [utils.zigzag(block) for block in Cb_blocks]
    Cr_flattened_blocks = [utils.zigzag(block) for block in Cr_blocks]

    list_to_compress = Y_flattened_blocks + Cb_flattened_blocks + Cr_flattened_blocks
    list_to_compress = np.array(list_to_compress).flatten()

    # TODO Cette ligne sera à enlever après la quantification
    list_to_compress = list(np.array(list_to_compress).astype(np.uint8))

    # Encodage Huffman et RLE
    frequencies = {}
    for element in list_to_compress:
        if str(element) in frequencies:
            frequencies[str(element)] += 1
        else:
            frequencies[str(element)] = 1
    for element in frequencies:
        frequencies[element] /= len(list_to_compress)
    huffman_codes = Huffman.huffman(frequencies)
    bits = ''
    for element in list_to_compress:
        bits += huffman_codes[str(element)]
    max_length_for_sequence = 32  # valeur maximale de répétitions dans RLE
    bits = RLE.rle_encode(bits, max_length_for_sequence)
    # Calcul du taux de compression
    compressed_size = len(bits)
    original_size = w * h * 3 * 8
    print('Taux de compression JPEG est: ', (1 - compressed_size/original_size))

    # Décodage Huffman et RLE
    # On saute cette partie puisque les étudiants n'ont pas à le faire et on peut réutiliser les variables d'avant

    # dé-aplatit les blocs avec le même pattern zig-zag
    Y_blocks = [utils.zigzag_inverse(sequence) for sequence in Y_flattened_blocks]
    Cb_blocks = [utils.zigzag_inverse(sequence) for sequence in Cb_flattened_blocks]
    Cr_blocks = [utils.zigzag_inverse(sequence) for sequence in Cr_flattened_blocks]

    # TODO quantification inverse
    Y_blocks = quantification_inv(Y_blocks, quantification_matrix)
    Cb_blocks = quantification_inv(Cb_blocks, quantification_matrix)
    Cr_blocks = quantification_inv(Cr_blocks, quantification_matrix)

    # TODO DCT inverse
    Y_blocks = dct_inv(Y_blocks)
    Cb_blocks = dct_inv(Cb_blocks)
    Cr_blocks = dct_inv(Cr_blocks)


    # On obtient un array 2D à partir d'une liste de blocs 8x8 et largeur, hauteur
    Y = utils.nxn_blocks_to_array(Y_blocks, int(Y.shape[0] / 8), int(Y.shape[1] / 8), n=8)
    Cb = utils.nxn_blocks_to_array(Cb_blocks, int(Cb.shape[0] / 8), int(Cb.shape[1] / 8), n=8)
    Cr = utils.nxn_blocks_to_array(Cr_blocks, int(Cr.shape[0] / 8), int(Cr.shape[1] / 8), n=8)
    # Upsample
    Y, Cb, Cr = utils.upsample(Y, Cb, Cr, subsample=SUBSAMPLING)
    # YCbCr vers RGB
    r, g, b = utils.YCbCr_to_rgb(Y, Cb, Cr)
    # Concatène les channels en ordre BGR pour OpenCV
    bgr_float_matrix = np.stack([b, g, r], axis=-1)
    # Conversion float array en image (type uint 8 et clip [0 - 255] pour les erreurs d'arrondis.
    bgr_image = utils.float_to_image(bgr_float_matrix)
    # On enlève le zero padding
    decompressed = bgr_image[:w, :h, ...]
    # Affiche image
    # cv2.imshow('Image jpgeg originale', image_original)
    # cv2.imshow('Image jpgeg decompressee', decompressed)
    # cv2.waitKey()

    cv2.imwrite(output_image_path, decompressed)
