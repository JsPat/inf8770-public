import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as dctpack
import os
from utils import LZW
from utils import utils
import math


def dwt(image):
    # Inspire du code de DWT des notes de cours
    f1 = (image[:,::2] + image[:,1::2])/2
    fh = (image[:,::2] - image[:,1::2])/2
    f11 = (f1[::2,:] + f1[1::2,:])/2
    f1h = (f1[::2,:] - f1[1::2,:])/2
    fh1 = (fh[::2,:] + fh[1::2,:])/2
    fhh = (fh[::2,:] - fh[1::2,:])/2
    return (f11, f1h, fh1, fhh)

def inv_dwt(f11, f1h, fh1, fhh, image_lenght, image_width):
    image_dwt = np.zeros((image_lenght, image_width*2))
    f1 = np.zeros((len(f11), len(f11[0])*2))
    fh = np.zeros((len(fh1), len(fh1[0])*2))

    for i in range(0, len(f11)):
        for j in range(0, len(f11[0])):
            f1[i, j*2] = f11[i, j] + f1h[i, j]
            f1[i, j*2 + 1] = f11[i, j] - f1h[i, j]
    for i in range(0, len(fh1)):
        for j in range(0, len(fh1[0])):
            fh[i, j*2] = fh1[i, j] + fhh[i, j]
            fh[i, j*2 + 1] = fh1[i, j] - fhh[i, j]

    # Inspire du code de DWT des notes de cours
    for i in range(0, len(f1)):
        for j in range(0, len(f1[0])):
            image_dwt[2*i, j] = f1[i, j]
            image_dwt[2*i + 1, j] = f1[i, j]
    for i in range(0, len(fh)):
        for j in range(0, len(fh[0])):
            image_dwt[2*i, j] += fh[i, j]
            image_dwt[2*i + 1, j] -= fh[i, j]
    return image_dwt

def quantification(f11, f1h, fh1, fhh, deadZoneLength, stepLength):
    bloc_image = np.concatenate((f11, f1h, fh1, fhh))

    # Apply dead zone quantifizer
    for i, _ in enumerate(bloc_image):
        for j, _ in enumerate(bloc_image[i]):
            negative = bloc_image[i][j] < 0
            if negative:
                bloc_image[i][j] = -bloc_image[i][j]
            if bloc_image[i][j] > deadZoneLength:
                bloc_image[i][j] = math.floor((bloc_image[i][j] - deadZoneLength) / stepLength)
            else:
                bloc_image[i][j] = 0
            if negative:
                bloc_image[i][j] = -bloc_image[i][j]
    return bloc_image

def inv_quantification(image, deadZoneLength, stepLength):
    for i, _ in enumerate(image):
        for j, _ in enumerate(image[i]):
            negative = image[i][j] < 0
            if negative:
                image[i][j] = -image[i][j]
            if image[i][j] == 0:
                image[i][j] = deadZoneLength / 2
            else:
                image[i][j] = (image[i][j] * stepLength) + deadZoneLength
            if negative:
                image[i][j] = -image[i][j]
    l = int(len(image)/4)
    return (image[:l], image[l:2*l], image[2*l:3*l], image[3*l:])

def jpeg2000(SUBSAMPLING, image_path, output_image_path):
    #  On s'assure que l'image existe
    assert os.path.exists(image_path), "L'image n'existe pas"
    # On lit l'image
    image = cv2.imread(image_path)
    # largeur et hauteur
    w, h = image.shape[:2]
    # On fait un zero padding pour s'assurer que l'image est un multiple de 4
    image = utils.make_multiple_of_n(image, 4)
    # Chaque channel en tant que floats
    r, g, b = utils.img_to_rgb_floats(image)
    # Conversion YUV
    Y, U, V = utils.rgb_to_YUV(r, g, b)
    # Subsample
    Y, U, V = utils.subsample(Y, U, V, subsample=SUBSAMPLING)

    # TODO DWT - OK
    (f11_Y, f1h_Y, fh1_Y, fhh_Y) = dwt(Y)
    (f11_U, f1h_U, fh1_U, fhh_U) = dwt(U)
    (f11_V, f1h_V, fh1_V, fhh_V) = dwt(V)

    # TODO quantification - OK
    deadZoneLength, stepLength = 5, 2
    Y_quant = quantification(f11_Y, f1h_Y, fh1_Y, fhh_Y, deadZoneLength, stepLength)
    U_quant = quantification(f11_U, f1h_U, fh1_U, fhh_U, deadZoneLength, stepLength)
    V_quant = quantification(f11_V, f1h_V, fh1_V, fhh_V, deadZoneLength, stepLength)

    # LZW
    Y_compresse = LZW.compress(str(Y_quant.tolist()))
    U_compresse = LZW.compress(str(U_quant.tolist()))
    V_compresse = LZW.compress(str(V_quant.tolist()))

    # calcul du taux de compression

    longueur_compressee = len(Y_compresse) + len(U_compresse) + len(V_compresse)
    longueur_originale = w * h * 3  # un octet par valeur (uint8)
    print('Taux de compression JPEG2000 est: ', (1 - longueur_compressee/longueur_originale))

    # LZW inverse
    Y = np.array(eval(LZW.decompress(Y_compresse)))
    U = np.array(eval(LZW.decompress(U_compresse)))
    V = np.array(eval(LZW.decompress(V_compresse)))

    # TODO quantification inverse - OK
    (f11_Y, f1h_Y, fh1_Y, fhh_Y) = inv_quantification(Y, deadZoneLength, stepLength)
    (f11_U, f1h_U, fh1_U, fhh_U) = inv_quantification(U, deadZoneLength, stepLength)
    (f11_V, f1h_V, fh1_V, fhh_V) = inv_quantification(V, deadZoneLength, stepLength)

    # TODO DWT Inverse
    Y = inv_dwt(f11_Y, f1h_Y, fh1_Y, fhh_Y, len(Y), len(Y[0]))
    U = inv_dwt(f11_U, f1h_U, fh1_U, fhh_U, len(U), len(U[0]))
    V = inv_dwt(f11_V, f1h_V, fh1_V, fhh_V, len(V), len(V[0]))

    #upsample
    Y, U, V = utils.upsample(Y, U, V, subsample=SUBSAMPLING)

    # YUV vers RGB
    r, g, b = utils.YUV_to_rgb(Y, U, V)

    # Concatène les channels en ordre BGR pour OpenCV
    bgr_float_matrix = np.stack([b, g, r], axis=-1)
    # Conversion float array en image (type uint 8 et clip [0 - 255] pour les erreurs d'arrondis.
    bgr_image = utils.float_to_image(bgr_float_matrix)
    # On enlève le zero padding
    decompressed = bgr_image[:w, :h, ...]
    # Affiche image
    # cv2.imshow('Image JPEG2000 originale', image)
    # cv2.imshow('Image JPEG2000 decompressee', decompressed)
    # cv2.waitKey()

    cv2.imwrite(output_image_path, decompressed)







