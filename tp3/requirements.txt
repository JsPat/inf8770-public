matplotlib==3.4.1
opencv_python==4.5.1.48
tqdm==4.60.0
prettytable==2.0.0
numpy==1.20.2
