# importing the necessary libraries
import cv2
import numpy as np
from prettytable import PrettyTable
import tqdm as pb


def distance_between_frame(frame1, frame2):
    hist1 = get_all_hist(frame1)
    hist2 = get_all_hist(frame2)

    total = 0
    dist = [0,0,0]

    # calculate distance
    for i, h in enumerate(hist1):
        for j in range(0, 3): # for rgb
            d = distance(hist2[i][j], h[j])
            dist[j] += d
            total += d
    
    return total


def distance(hist1, hist2):
    # https://stackoverflow.com/questions/1401712/how-can-the-euclidean-distance-be-calculated-with-numpy
    return np.linalg.norm(hist1-hist2)

def get_hist(frame):
    colors = ('b','g','r') # all possible colors
    hist = [None, None, None]

    for i, _ in enumerate(colors):
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_begins/py_histogram_begins.html
        hist[i] = cv2.calcHist([frame],[i],None,[16],[0,256])

    return hist

def split_frame(frame):
    # https://stackoverflow.com/questions/43808353/how-to-split-a-video-frame-into-9-pieces
    height, _, _ = frame.shape
    sub_frame = []

    sub_height = int(height / 2)

    for i  in range(0, 2):
        sub_frame.append(frame[i * sub_height: (i + 1) * sub_height, :])

    return sub_frame

def get_all_hist(frame):
    split_frames = split_frame(frame)
    all_hist = []

    for sub_frame in split_frames:
        all_hist.append(get_hist(sub_frame))

    return all_hist

def parseFade(fades):
    finalFade = []
    start = 0
    end = 0

    for fade in fades:
        if start == 0 and end == 0:
            start = fade[0]
            end = fade[1]
        else:
            if fade[0] - end == 1:
                end = fade[1]
            else:
                finalFade.append([start, end])
                start = fade[0]
                end = fade[1]

    if start != 0 and end != 0:
        finalFade.append([start, end])
    
    return finalFade
        

def main():
    table_cut = PrettyTable(["frame"])
    table_cut.align["frame"] = "l"

    table_fade = PrettyTable(["start", "end"])
    table_fade.align["start"] = "l"
    table_fade.align["end"] = "l"

    high_threshold = 700_000
    low_threshold = 350_000

    # Creating a VideoCapture object to read the video
    cap = cv2.VideoCapture('../video/ski_cross.mp4')

    # obtain first frame
    _, previous_frame = cap.read()

    first_frame_fade = None
    evaluating_fade = False
    start_fade = 0
    fade_found = []
    cut_found = []

    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    # Loop untill the end of the video
    for n_frame in pb.tqdm(range(2, length)):

        _, current_frame = cap.read() # get the frame
        frame_dist = distance_between_frame(previous_frame, current_frame) # get distance

        # fade are not longer then 25 ish frames
        if evaluating_fade and n_frame - start_fade > 25:
            evaluating_fade = False

        # possible fade
        if frame_dist > low_threshold :
            if not evaluating_fade: 
                evaluating_fade = True
                first_frame_fade = previous_frame
                start_fade = n_frame

        # check if the fade is a real one
        elif evaluating_fade:
                fade_dist = distance_between_frame(first_frame_fade, current_frame)
                
                if fade_dist > high_threshold:
                    fade_found.append([start_fade, n_frame])
                    evaluating_fade = False

        previous_frame = current_frame



    # append fade togheter if 2 are following each other
    fade_found = parseFade(fade_found)

    for fade in fade_found:
        # found cut that were flag as fade
        if fade[1] - fade[0] == 1:
            table_cut.add_row([fade[0]])
        else:
            table_fade.add_row(fade)

    print(table_cut)
    print(table_fade)


if __name__ == "__main__":
    main()


